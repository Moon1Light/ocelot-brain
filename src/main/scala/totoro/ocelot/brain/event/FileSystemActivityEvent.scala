package totoro.ocelot.brain.event

case class FileSystemActivityEvent(address: String) extends Event
