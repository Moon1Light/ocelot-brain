package totoro.ocelot.brain.entity.traits

import totoro.ocelot.brain.Ocelot
import totoro.ocelot.brain.nbt.ExtendedNBT._
import totoro.ocelot.brain.nbt.persistence.NBTPersistence
import totoro.ocelot.brain.nbt.{NBT, NBTTagCompound}
import totoro.ocelot.brain.util.Persistable
import totoro.ocelot.brain.workspace.Workspace

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

/**
  * Describes an entity with internal container, which can hold other entities.
  * Such as computer case.
  */
trait Inventory extends WorkspaceAware with Persistable {
  /**
    * Returns the collection of entites indide of the container
    */
  val inventory: mutable.ListBuffer[Entity] = ListBuffer.empty

  /**
    * Put entity in the inventory
    */
  def add(entity: Entity): Boolean = {
    if (!inventory.contains(entity)) {
      inventory += entity
      onEntityAdded(entity)
      true
    } else false
  }

  /**
    * Remove an entity from inventory
    */
  def remove(entity: Entity): Boolean = {
    if (inventory.contains(entity)) {
      inventory -= entity
      onEntityRemoved(entity)
      true
    } else false
  }

  def clear(): Boolean = {
    if (inventory.nonEmpty) {
      inventory.clear()
      true
    } else false
  }

  /**
    * Is called any time new entity is added to the inventory
    */
  def onEntityAdded(entity: Entity): Unit = {
    entity match {
      case e: WorkspaceAware => e.workspace = workspace
      case _ =>
    }
  }

  /**
   * Is called any time new entity is removed from the inventory
   */
  def onEntityRemoved(entity: Entity): Unit = {
    entity match {
      case e: WorkspaceAware => e.workspace = null
      case _ =>
    }
  }

  override def onWorkspaceChange(newWorkspace: Workspace): Unit = {
    super.onWorkspaceChange(newWorkspace)
    inventory.foreach {
      case e: WorkspaceAware => e.workspace = newWorkspace
      case _ =>
    }
  }

  // ----------------------------------------------------------------------- //

  private final val InventoryTag = "inventory"

  override def load(nbt: NBTTagCompound, workspace: Workspace) {
    super.load(nbt, workspace)
    nbt.getTagList(InventoryTag, NBT.TAG_COMPOUND).foreach { nbt: NBTTagCompound =>
      NBTPersistence.load(nbt, workspace) match {
        case entity: Entity =>
          add(entity)
        case _ =>
          Ocelot.log.error("Some problems parsing an entity from NBT tag: " + nbt)
      }
    }
  }

  override def save(nbt: NBTTagCompound) {
    super.save(nbt)
    nbt.setNewTagList(InventoryTag,
      inventory.map { entity =>
        NBTPersistence.save(entity)
      }
    )
  }
}
